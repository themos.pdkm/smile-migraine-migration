package models

import "time"

type Something struct {
	ID   uint `gorm:"primeryKey; index; auto_increment"`
	Name string

	// HeadacheSymptoms []HeadacheSymptom `gorm:"ForeignKey:SomethingID;AssociationForeignKey:ID"`
}

type Medicine struct {
	MedicineID uint `gorm:"primeryKey; column:medicine_id"`
	Name       string

	// HeadacheSymptoms []HeadacheSymptom `gorm:"ForeignKey:MedicineID;AssociationForeignKey:MedicineID"`
}

type HeadacheSymptom struct {
	ID         uint       `gorm:"primeryKey; index; auto_increment"`
	SymptomEtc string     `gorm:"not null; varchar(120)"`
	Label      string     `gorm:"type:text"`
	AddDtm     *time.Time `gorm:"default:null"`
	UpdateDtm  *time.Time `gorm:"default:null"`

	SymptomID       uint `gorm:"not null"`
	HeadacheScoreID uint `gorm:"not null"`
	MemberID        uint `gorm:"not null"`
	MedicineID      uint `gorm:"not null"`
	SomethingID     uint `gorm:"not null"`

	Something *Something `gorm:"foreignKey:SomethingID"`
	Medicine  *Medicine  `gorm:"foreignKey:MedicineID; column:medicine_id"`
}
