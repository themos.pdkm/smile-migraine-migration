package models

import (
	"fmt"
	"os"

	"gorm.io/driver/mysql"
	gorm "gorm.io/gorm"
)

var MySql *gorm.DB

func ConnectMySqlDB() error {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		os.Getenv("MYSQL_USERNAME"),
		os.Getenv("MYSQL_PASSWORD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_PORT"),
		os.Getenv("MYSQL_DATABASE_NAME"),
	)

	fmt.Println(">>", connectionString)
	var err error
	MySql, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{})

	migrateDB()

	return err
}

func migrateDB() {
	MySql.Debug().AutoMigrate(&HeadacheSymptom{})
	MySql.Debug().AutoMigrate(&Something{})
}
