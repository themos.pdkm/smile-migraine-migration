package main

import (
	"flag"
	"fmt"
	"log"

	"sm-structure/models"

	"github.com/joho/godotenv"
)

var env string

var initDB = flag.Bool("initDB", false, "initialize data")

func init() {
	flag.Parse()

	if env == "" {
		env = "development"
	}

	if err := godotenv.Load(".env"); err != nil {
		fmt.Println("Error loading .env file")
	}

	if err := models.ConnectMySqlDB(); err != nil {
		log.Panic("Cannot connect [MySQL] database ", err)
	}
	fmt.Printf("OK [%s]\n", env)
}

func main() {

}
